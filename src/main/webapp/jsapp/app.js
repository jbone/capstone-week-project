'use strict';

// Declare app level module which depends on views, and components

var mainApp = angular.module('mainApp', ['ngRoute', 'ngResource']);


mainApp.config(['$routeProvider',function($routeProvider) {
	$routeProvider.when('/main',
			{
				templateUrl: 'templates/main.tpl.html',
				controller: 'mainCtrl'
			}
	);
	
	$routeProvider.when('/player',
	{
		templateUrl: 'templates/player.tpl.html',
		controller: 'playerCtrl'//,
		// resolve: {
		// 			players:function(playerService){
		// 				return playerService.getPlayers();
		// 			}
		// 		}
		 	}
	);

	$routeProvider.when('/details/:name',
	{
		templateUrl: 'templates/details.tpl.html',
		
	}
		)
	
	$routeProvider.otherwise({redirectTo: 'main'});
}]);

