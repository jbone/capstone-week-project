mainApp.controller('playerCtrl', function($scope, $log, playerService) {
	$log.info("launched main controller");
	$scope.players = playerService.getPlayers();
});