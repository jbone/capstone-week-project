mainApp.factory('playerService', function($resource) {
	
	var playersRes = $resource('/capstone-review/app/players');
	
	return {
		getPlayers: getPlayers
	};
	
	function getPlayers() {
        //var players = {name: "test"};
		//return players;
		return playersRes.query();
	}
	
	
});